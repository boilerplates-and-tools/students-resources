const axios = require('axios');

const backend = axios.create({
  baseURL: 'http://localhost:5050/',
  timeout: 1000,
});

export { backend };
