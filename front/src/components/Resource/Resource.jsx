import React from 'react';
import { Card } from 'semantic-ui-react';

const Resource = ({ data: { title, description, contributorId, link } }) => (
  <Card
    href={link}
    header={title}
    meta={`Wilder #${contributorId}`}
    description={description}
  />
);
export default Resource;
