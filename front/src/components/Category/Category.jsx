import React from 'react';
import { Divider } from 'semantic-ui-react';

import Resource from '../Resource/Resource';

const Category = ({ data: { name, resources } }) => (
  <div>
    <Divider horizontal>{name}</Divider>
    <div className="ui cards">
      {resources.map((resource) => {
        return <Resource key={resource.id} data={resource} />;
      })}
    </div>
  </div>
);
export default Category;
