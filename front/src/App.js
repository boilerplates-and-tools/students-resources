import React from 'react';
import 'semantic-ui-css/semantic.min.css';
import { backend } from './conf';
import Category from './components/Category/Category';
import BacASable from './components/BacASable/BacASable';

class App extends React.Component {
  constructor(props) {
    super(props);
    this.state = { resources: [], categories: [] };
  }
  componentDidMount() {
    backend
      .get('/resources')
      .then((response) => {
        const resources = response.data;
        this.setState({ resources });
      })
      .catch((error) => {
        console.log(`Nope : ${error}`);
      });
    backend
      .get('/category')
      .then((response) => {
        const categories = response.data;
        this.setState({ categories });
      })
      .catch((error) => {
        console.log(`Nope : ${error}`);
      });
  }
  render() {
    const { resources, categories } = this.state;
    return (
      <div className="App">
        <h1>Students Resources : {this.state.resources.length}</h1>
        {categories.map((category) => {
          category.resources = resources.filter(
            (resource) => category.id === resource.categoryId,
          );
          return <Category key={category.id} data={category} />;
        })}
      </div>
    );
  }
}

export default App;
