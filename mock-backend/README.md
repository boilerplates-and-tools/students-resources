# Students-resources mock-Backend

The idea is to get a first, time-limited server to kickstart our development.
We are using `node-server`, a package designed specifically to this end.

To launch the server, just type :
`json-server --watch resources.json --port 5050`

## Links to add
- Arlen
  - http://exploringjs.com/impatient-js/toc.html
  - https://blog.xebia.fr/2017/11/14/asyncawait-une-meilleure-facon-de-faire-de-lasynchronisme-en-javascript/ 
  - https://developer.mozilla.org/fr/docs/Web/JavaScript/Reference/Instructions/async_function 
  - https://www.vagrantup.com/intro/index.html
  - https://serverless-stack.com/#table-of-contents 
  - https://serverless.com/framework/docs/providers/aws/ 
  - https://serverless.com/framework/docs/providers/aws/guide/quick-start/ 
  - https://serverless.com/framework/docs/providers/aws/guide/serverless.yml/ 
  - https://github.com/dherault/serverless-offline
  - https://www.npmjs.com/package/serverless-dynamodb-local
  - https://github.com/serverless-heaven/serverless-webpack 
  - https://docs.aws.amazon.com/AWSJavaScriptSDK/latest/ 
  - https://redux.js.org/basics/basic-tutorial 
  - https://redux-saga.js.org/docs/introduction/BeginnerTutorial.html
- Emilie
  - https://react.explore-tech.org/
- Nicolas
  - https://github.com/i0natan/nodebestpractices
 
- Unknown
    - https://buzut.fr/101-commandes-indispensables-sous-linux/
    - https://learngitbranching.js.org/
    - https://www.linux.com/learn/intro-to-linux/2017/9/security-tools-check-viruses-and-malware-linux
    -  https://scotch.io/bar-talk/6-popular-css-frameworks-to-use-in-2019/amp?__twitter_impression=true
    - https://scotch.io/tutorials/using-create-react-app-v2-and-typescript
    - https://blog.eleven-labs.com/fr/react-apollo-le-local-state-management-une-alternative-a-redux-contextapi/
    - https://hackernoon.com/getting-started-with-react-redux-1baae4dcb99b